@WeatherReport
Feature: validating the Weather Stack for various cities

  Scenario Outline: Looking up the Weather Report
    When I send the request for <City> with mode <Mode> weather report
    Then I validate city type <Type> and timezone <timeZone> and code <weather_code>
    Examples:
      |Mode | City | Type | timeZone | weather_code |
      | current | New York | City | America/New_York | 122 |
      | current | Florida  | City | America/Bogota   | 116 |
      | current | Alaska   | City | America/Mexico_City | 353 |

  Scenario Outline: Looking up the Weather Report with negative
    When I send the request for <City> with mode <Mode> weather report
    Then I validate error code <error_code> and info <err_info>
    Examples:
      |Mode | City | error_code | err_info |
      | current | abc | 615 | Your API request failed. Please try again or contact support.|
      | current | xyz  | 615 | Your API request failed. Please try again or contact support.|
