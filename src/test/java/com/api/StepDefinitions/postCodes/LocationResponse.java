package com.api.StepDefinitions.postCodes;

public class LocationResponse {
    public static final String COUNTRY = "'country'";
    public static final String CITY = "'state abbreviation'";
    public static final String FIRST_PLACE_NAME = "'places'[0].'place name'";
    public static final String FIRST_POST_CODE = "'places'[0].'post code'";

}