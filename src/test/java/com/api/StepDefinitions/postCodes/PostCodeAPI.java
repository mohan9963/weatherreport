package com.api.StepDefinitions.postCodes;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class PostCodeAPI {

    private static String LOCATION_BY_POST_CODE_AND_COUNTRY = "http://api.zippopotam.us/{country}/{postcode}";
    private static String LOCATION_BY_POST_CODE_AND_STATE_AND_COUNTRY = "http://api.zippopotam.us/{country}/{state}/{city}";

    @Step("Get location by postcode {0} in country {1}")
    public void fetchLocationByPostCodeAndCountry(String postcode, String country) {
        SerenityRest.given()
                .pathParam("postcode", postcode)
                .pathParam("country", country)
                .get(LOCATION_BY_POST_CODE_AND_COUNTRY);
    }

    @Step("Get location by city {0} and state {1} in country {2}")
    public void fetchLocationByStateAndCityAndCountry(String city, String state, String country) {
        SerenityRest.given()
                .pathParam("country", country)
                .pathParam("state", state)
                .pathParam("city", city)
                .get(LOCATION_BY_POST_CODE_AND_STATE_AND_COUNTRY);
    }
}
