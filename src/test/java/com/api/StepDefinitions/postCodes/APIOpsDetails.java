package com.api.StepDefinitions.postCodes;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import net.thucydides.core.annotations.Steps;
import static org.hamcrest.Matchers.equalTo;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class APIOpsDetails {
    @Steps
    PostCodeAPI postCodeAPI;

    @When("I look up a post code {word} for country code {word}")
    public void lookUpAPostCode(String postCode, String country) {
        postCodeAPI.fetchLocationByPostCodeAndCountry(postCode, country);
    }

    @Then("the resulting location should be {} in {}")
    public void theResultingLocationShouldBe(String placeName, String country) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body(LocationResponse.COUNTRY, equalTo(country)));
        restAssuredThat(response -> response.body(LocationResponse.FIRST_PLACE_NAME, equalTo(placeName)));
    }

    @When("I look up a city {word} and {word} for country code {word}")
    public void lookUpCity(String city, String state, String country) {
        postCodeAPI.fetchLocationByStateAndCityAndCountry(city, state, country);
    }

    @Then("the resulting postcode should be {} in {}")
    public void theResultingPostCodeShouldBe(String postCode, String city) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body(LocationResponse.CITY, equalTo(city)));
        restAssuredThat(response -> response.body(LocationResponse.FIRST_POST_CODE, equalTo(postCode)));
    }
}
