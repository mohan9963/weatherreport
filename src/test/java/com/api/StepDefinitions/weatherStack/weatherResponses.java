package com.api.StepDefinitions.weatherStack;

public class weatherResponses {
    public static final String LOCATION_NAME = "'location'.'name'";
    public static final String CITY_TYPE = "'request'.'type'";
    public static final String TIMEZONE_ID = "'location'.'timezone_id'";
    public static final String WEATHER_CODE = "'current'.'weather_code'";
}
