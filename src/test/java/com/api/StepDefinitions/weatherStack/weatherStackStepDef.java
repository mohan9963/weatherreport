package com.api.StepDefinitions.weatherStack;

import com.api.StepDefinitions.postCodes.LocationResponse;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.ResponseBody;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class weatherStackStepDef {
    @Steps(shared = true)
    fetchWeatherReport FetchWeatherReport;

    @When("I send the request for {} with mode {} weather report")
    public void getWeatherReport(String City, String Mode){
        FetchWeatherReport.pullWeatherReportByCity(Mode, City);
    }

    @Then("I validate city type {} and timezone {} and code {word}")
    public void theResultingLocationShouldBe(String type, String timeZone, String weatherCode) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body(weatherResponses.CITY_TYPE, equalTo(type)));
        restAssuredThat(response -> response.body(weatherResponses.TIMEZONE_ID, equalTo(timeZone)));
        restAssuredThat(response -> response.body(weatherResponses.WEATHER_CODE, notNullValue()));

    }

}
