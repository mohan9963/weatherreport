package com.api.StepDefinitions.weatherStack;

import io.restassured.RestAssured;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class fetchWeatherReport {
    private static String api_key = "50aab5f21b9f160c094b4a92a3ea2da8";
    private static String CURRENT_WEATHER_REPORT_BY_CITY = "http://api.weatherstack.com/{mode}";


    @Step("Get weather report for city {0} with {1}")
    public void pullWeatherReportByCity(String mode, String city) {
        SerenityRest.given()
                .pathParam("mode", mode)
                .queryParam("access_key", api_key)
                .queryParam("query", city)
                .get(CURRENT_WEATHER_REPORT_BY_CITY);

    }
}
